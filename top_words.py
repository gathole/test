import sys
from collections import Counter
import operator

def foo(data_list, word_input):
	w_list = [x for x in data_list if x.startswith(word_input)]
	word_count_dict = dict(Counter(w_list))
	vl = list(word_count_dict.itervalues())
	fl = [x[0] for x in sorted(word_count_dict.items(),key=operator.itemgetter(1), reverse=True)]
	print "######## Output #######"
	if fl:
		for f in fl:
			print f
	else:
		print None


if __name__ == "__main__":
	count = 0

	try:
		count=int(raw_input())
	except ValueError:
    		print "Not a number"

	data_list = []
	for i in range(count):
		line = raw_input()
		data_list.append(line)
	
	word_input = raw_input()
	foo(data_list, word_input)
